<?php
/**
 * Copyright (C) 2016  Kristijan Lenković
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * UPZUstanove
 *
 * Klasa dohvaća sve ustanove sa stranice http://bib.irb.hr/lista-ustanova?period=2007
 *
 * @author     Kristijan Lenković <klenkovic@uniri.hr>
 * @license    https://www.gnu.org/licenses/gpl-3.0.en.html  GNU GPLv3
 */
class UPZUstanove
{

    const URL = 'http://bib.irb.hr/lista-ustanova?period=2007';
    const XPATH = '//ol/li';

    private $ustanove;

    public function __construct()
    {
        $this->_dohvatiUstanove(self::URL, self::XPATH);
    }

    private function _dohvatiUstanove($url, $xpath)
    {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTMLFile($url);
        $domxpath = new DOMXPath($dom);
        $this->ustanove = array();
        $ustanoveDOM = $domxpath->query($xpath);

        foreach($ustanoveDOM as $u){
            $ustanove = preg_split('/\s+(?=\(\d+\)$)/', $u->nodeValue);
            $this->ustanove[] = array(
                "naziv" => trim($ustanove[0]),
                "id" => trim(str_replace(array( '(', ')' ), '', $ustanove[1]))
            );
        }
    }

    public function getUstanove()
    {
        return $this->ustanove;
    }

}