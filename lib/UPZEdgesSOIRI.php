<?php
/**
 * Copyright (C) 2016  Kristijan Lenković
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * UPZEdgesSOIRI
 *
 * UPZEdgesSOIRI klasa je klasa za parsiranje web stranice
 * http://bib.irb.hr/lista-ustanove?sifra=318&period=2007 za potrebe kolegija
 * "Upravljanje znanjem" u sklopu Odjela za Informatiku Sveučilišta u Rijeci.
 *
 * @author     Kristijan Lenković <klenkovic@uniri.hr>
 * @license    https://www.gnu.org/licenses/gpl-3.0.en.html  GNU GPLv3
 */
class UPZEdgesSOIRI extends UPZEdges
{
    const URL = 'http://bib.irb.hr/lista-radova?sif_ust=318&period=2007';
    const XPATH = '//td[@height="92" and @valign="top"]/table[@width="100%"and @border="0" and @cellspacing="2" and @cellpadding="0"]/tr/td[@class="text"]';

    const ISPRAVCI_AUTORA = "../ispravci/ispravciAutoraSOIRI.php";
    const ISPRAVCI_IMENA = "../ispravci/ispravciImenaSOIRI.php";

    public function __construct()
    {
        $this->_dohvatiRadove(self::URL, self::XPATH);
        $this->_filtriranjeRadova();
        $this->_dohvatiAutoreRadova();
        $this->_dohvatiPojedinacneAutore();
        $this->_napraviSuradnistva();
    }

    protected function _filtriranjeRadova()
    {
        $filtriraniRadovi = array();
        foreach ($this->radovi as $broj => $rad) {
            $filter = trim(trim($rad->nodeValue, " "));
            if ($filter == "" || $filter == " " || $filter == "&nbsp;" || $broj < 3 || ($broj > 3 && $broj < 11)) {
                continue;
            }
            $filtriraniRadovi[] = $filter;
        }
        $this->radovi = $filtriraniRadovi;
        $this->brRadova = count($this->radovi);
    }

    protected function _dohvatiAutoreRadova()
    {
        require(self::ISPRAVCI_AUTORA);
        $this->_odvojiSveAutore($ispravciAutora);
    }

    protected function _dohvatiPojedinacneAutore()
    {
        require(self::ISPRAVCI_IMENA);
        $this->_odvojiAutore($ispravciImena);
    }


}