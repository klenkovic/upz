<?php
/**
 * Copyright (C) 2016  Kristijan Lenković
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * UPZEdges
 *
 * UPZEdges klasa je bazična klasa za generiranje Edges datoteka sa web stranica
 * http://bib.irb.hr/lista-ustanova?period=2007 za potrebe kolegija
 * "Upravljanje znanjem" u sklopu Odjela za Informatiku Sveučilišta u Rijeci.
 * Kako bi se generator iskoristio potrebno je iskoristiti chiled klasu
 * koja parsira jednu od navedenih ustanova.
 *
 * @author     Kristijan Lenković <klenkovic@uniri.hr>
 * @license    https://www.gnu.org/licenses/gpl-3.0.en.html  GNU GPLv3
 */
abstract class UPZEdges
{

    protected $radovi;
    protected $autoriRadova;
    protected $autoriRadovaSViseOdJednogAutora;
    protected $sviAutori;
    protected $brRadova;

    private $brRadovaSJednimAutorom;
    private $brRadovaSViseAutora;
    private $suradnistva;
    private $edges;

    private $jedinstveniAutori;

    public function getSviAutori()
    {
        return $this->sviAutori;
    }

    public function getEdges()
    {
        return $this->edges;
    }

    public function getSuradnistva()
    {
        return $this->suradnistva;
    }

    public function getBrRadova()
    {
        return $this->brRadova;
    }

    public function getBrRadovaSJednimAutorom()
    {
        return $this->brRadovaSJednimAutorom;
    }

    public function getBrRadovaSViseAutora()
    {
        return $this->brRadovaSViseAutora;
    }

    public function getBrAutora()
    {
        return count($this->sviAutori);
    }

    public function getBrSuradnistva()
    {
        return count($this->suradnistva);
    }

    abstract protected function _filtriranjeRadova();

    abstract protected function _dohvatiAutoreRadova();

    abstract protected function _dohvatiPojedinacneAutore();

    protected function _napraviSuradnistva()
    {
        $this->_izbrisiAutoreSJednimAutorom();
        $kombi = new Math_Combinatorics();
        $kombinacijeAutora = array();
        foreach ($this->autoriRadovaSViseOdJednogAutora as $rad) {
            $kombinacijeAutora[] = $kombi->combinations($rad, 2);
        }
        $mergani = array();
        foreach ($kombinacijeAutora as $rad) {
            $mergani = array_merge($mergani, $rad);
        }
        for ($i = 0; $i < count($mergani); $i++) {
            sort($mergani[$i], SORT_STRING);
        }
        $this->suradnistva = array_unique($mergani, SORT_REGULAR);
        sort($this->suradnistva);
        for ($i = 0; $i < count($this->suradnistva); $i++) {
            $this->suradnistva[$i][2] = 0;
            foreach ($mergani as $rad) {
                if ($this->suradnistva[$i][0] == $rad[0] && $this->suradnistva[$i][1] == $rad[1]) {
                    $this->suradnistva[$i][2]++;
                }
            }
        }
        $this->_napraviEdges();
    }

    protected function _izbrisiAutoreSJednimAutorom()
    {
        $this->autoriRadovaSViseOdJednogAutora = array();
        foreach ($this->autoriRadova as $rad) {
            if (count($rad) > 1)
                $this->autoriRadovaSViseOdJednogAutora[] = $rad;
        }
        $this->brRadovaSJednimAutorom = count($this->autoriRadovaSViseOdJednogAutora);
        $this->brRadovaSViseAutora = $this->brRadova - $this->brRadovaSJednimAutorom;
    }

    private function _napraviEdges()
    {
        $this->edges = array();
        foreach ($this->suradnistva as $veza) {
            $this->edges[] = '"' . $veza[0] . '"' . ';' . '"' . $veza[1] . '"' . ';' . $veza[2];
        }
    }

    protected function _dohvatiRadove($url, $xpath)
    {
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTMLFile($url);
        //file_put_contents("str.html",$content);
        $domxpath = new DOMXPath($dom);
        $this->radovi = $domxpath->query($xpath);
    }

    protected function _odvojiSveAutore($ispravciAutora)
    {
        $autoriRadova = array();
        foreach ($this->radovi as $rad) {
            $autori = explode(".", $rad)[1];
            $autoriRadova[] = self::_ukloniHrZnakove(strtr($autori, $ispravciAutora));
        }
        $this->radovi = $autoriRadova;
    }

    private static function _ukloniHrZnakove($autori)
    {
        return str_replace(
            array("š", "đ", "č", "ć", "ž", "Š", "Đ", "Č", "Ć", "Ž"),
            array("s", "d", "c", "c", "z", "S", "D", "C", "C", "Z"),
            $autori
        );
    }

    protected function _odvojiAutore($ispravciImena)
    {
        $this->autoriRadova = array();
        $this->sviAutori = array();
        foreach ($ispravciImena as $krivo => $tocno) {
            $ispravciImena["/^" . $krivo . "$/"] = $tocno;
            unset($ispravciImena[$krivo]);
        }
        foreach ($this->radovi as $rad) {
            $autori = explode(";", $rad);
            for ($i = 0; $i < count($autori); $i++) {
                $autori[$i] = substr($autori[$i], 1);

                $autori[$i] = preg_replace(array_keys($ispravciImena), array_values($ispravciImena), $autori[$i]);
                if (!in_array($autori[$i], $this->sviAutori)) $this->sviAutori[] = $autori[$i];
            }
            $this->autoriRadova[] = $autori;
        }
        sort($this->sviAutori);
    }

    protected function _print_r($o)
    {
        echo "<pre>";
        print_r($o);
        echo "</pre>";
    }

    private function _setJedinstveniAutori(){
        $this->jedinstveniAutori = array();
        foreach ($this->suradnistva as $s){
            if (!in_array($s[0], $this->jedinstveniAutori)) $this->jedinstveniAutori[] = $s[0];
            if (!in_array($s[1], $this->jedinstveniAutori)) $this->jedinstveniAutori[] = $s[1];
        }
    }

    public function getVisJsNodes(){
        $this->_setJedinstveniAutori();
        $nodes = array();
        foreach ($this->jedinstveniAutori as $a){
            $nodes[] = array("id" => $a, "label" => $a);
        }
        return json_encode($nodes);
    }
    public function getVisJsEdges(){
        $edges = array();
        foreach ($this->suradnistva as $s){
            $edges[] = array("from" => $s[0], "to" => $s[1], "value" => $s[2]);
        }
        return json_encode($edges);
    }

}