<?php

$ispravciImena = [
    "Brkic, M" => "Brkic, Marija",
    "Brkic Marija" => "Brkic, Marija",
    "Skendzic, A" => "Skendzic, Aleksandar",
    "Asenbrener, Martina" => "Asenbrener Katic, Martina",
    "Martincic- Ipsic, Sanda" => "Martincic-Ipsic, Sanda",
    "Martincic–Ipsic, Sanda" => "Martincic-Ipsic, Sanda",
    "Martincic - Ipsic, Sanda" => "Martincic-Ipsic, Sanda",
    "Hoic- Bozic, Natasa" => "Hoic-Bozic, Natasa",
    "Hoic-Bozic Natasa" => "Hoic-Bozic, Natasa",
    "Holenko, Martina" => "Holenko Dlab, Martina",
    "Holenko Dlab Martina" => "Holenko Dlab, Martina",
    "Zdravko Dovedan Han" => "Dovedan Han, Zdravko",
    "Dovedan, Zdravko" => "Dovedan Han, Zdravko",
    "Mezak Jasminka" => "Mezak, Jasminka",
    "Ana, Mestrovic" => "Mestrovic, Ana",
    "mestrovic, Ana" => "Mestrovic, Ana",
    "Nacinovic, Lucia" => "Nacinovic Prskalo, Lucia",
    "Grubisa Zoran" => "Grubisa, Zoran",
    "Poscic, Partizia" => "Poscic, Patrizia"
];