function disableGenerator() {
    $("#generateButton").prop('disabled', true);
}

function enableGenerator() {
    $("#generateButton").prop('disabled', false);
}

function generatorWorking() {
    disableGenerator();
    $("#generateButtonTekst").text("Generiram...");
    $("#glyphiconGenerator").removeClass("glyphicon-play");
    $("#glyphiconGenerator").addClass("glyphicon-hourglass");
}

function generatorDone() {
    $("#generateButtonTekst").text("Generiraj");
    $("#glyphiconGenerator").removeClass("glyphicon-hourglass");
    $("#glyphiconGenerator").addClass("glyphicon-play");
}

function setGlobAnaliza(glob) {
    $.each(glob, function (key, value) {
        $("#" + key).text(value);
    });
}

function setLokAnaliza(stup, snag, medju){
    $.each(stup, function(i,cvor){
        br = i+1;
        $("#lok_stup_cvor_"+br).text(cvor[0].replace(/\"/g,''));
        $("#lok_stup_vrij_"+br).text(cvor[1]);
    });

    $.each(snag, function(i,cvor){
        br = i+1;
        $("#lok_snag_cvor_"+br).text(cvor[0].replace(/\"/g,''));
        $("#lok_snag_vrij_"+br).text(cvor[1]);
    });

    $.each(medju, function(i,cvor){
        br = i+1;
        $("#lok_medju_cvor_"+br).text(cvor[0].replace(/\"/g,''));
        $("#lok_medju_vrij_"+br).text(cvor[1]);
    });
}

function setViz(viz){
    $.each(viz, function (key, value) {
        $("#"+key).attr("src", "generated/"+value);
    });
}

function setVisJs(visjs){
    // create a network
    var container = document.getElementById('visjsMreza');
    var nodes = new vis.DataSet(JSON.parse(visjs.nodes));
    var edges = new vis.DataSet(JSON.parse(visjs.edges));

    // provide the data in the vis format
    var data = {
        nodes: nodes,
        edges: edges
    };
    var options = {};

    // initialize your network!
    network = new vis.Network(container, data, options);
}

function enableTabs() {
    $("#tabs li").removeClass("disabled");
    $("#tabs a").attr("data-toggle", "tab");
}

function getUstanove() {
    $.get("ajax/ustanove.php", "", function (d) {
        var output = [];
        $.each(d, function (index, ustanova){
            output.push('<option value="'+ustanova["id"]+'">'+ustanova["naziv"]+'</option>');
        });
        $("#ustanoveSelect").html(output.join(''));
        $("#ustanoveSelect option[value=318]").attr('selected','selected');
    });
}

$(document).ready(function () {

    $(".alert").hide();

    getUstanove();

    $(".close").click(function () {
        $(".alert").fadeOut();
    });

    $("#generateForm").submit(function (e) {
        e.preventDefault();
        $(".alert").hide();
        generatorWorking();
        formdata = $(this).serialize();
        grecaptcha.reset();
        $.post("ajax/generator.php", formdata, function (response) {
            //$("#debug").html(response);
            generatorDone();
            if (response == "recaptchaError") {
                $("#alertRecaptchaFail").fadeIn();
            } else {
                response = JSON.parse(response);
                $("#edgesPregled").val(response.edgesFormated);
                $("#dlLink").prop("href", "generated/" + response.edgesFile);
                setGlobAnaliza(response.globalnaAnaliza);
                setLokAnaliza(response.stup, response.snag, response.medju);
                setViz(response.viz);
                setVisJs(response.visjs);

                enableTabs();
                $("#alertUspjeh").fadeIn();
            }
        });

    });

});
