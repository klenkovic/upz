<?php

spl_autoload_register(function ($class) {
    include '../lib/' . $class . '.php';
});

$ustanove = new UPZUstanove();
header('Content-Type: application/json; charset=utf-8');
echo json_encode($ustanove->getUstanove());
