<?php

define("TIMEHASH", md5(microtime()));

function createEdgesFile($data, $ustanovaID)
{
    $path = "../generated/";
    $filename = $ustanovaID . "-" . TIMEHASH . ".edges";
    $fullpath = $path . $filename;
    file_put_contents($fullpath, $data);
    chmod($fullpath, 0664);
    return $filename;
}

function doPython3NetworkXscript($edgesFileName){

    $scriptPath = getcwd()."/../python/netx.py";
    $edgesFilePath = getcwd()."/../generated/".$edgesFileName;
    $cmd = "/usr/bin/python3 ".$scriptPath;
    $cmd = escapeshellcmd($cmd);
    $arg1 = escapeshellarg($edgesFilePath);
    $arg2 = escapeshellarg(TIMEHASH);
    $arg3 = escapeshellarg(getcwd()."/../generated/");
    $fullcmd = $cmd . " " . $arg1 . " " . $arg2 . " " . $arg3;
    $output = shell_exec($fullcmd);
    //echo "CMD: ".$fullcmd."<br>"."Output:".$output."<-END<br>";
    $output = json_decode($output);
    return $output;
}

$recaptchaSite = "6LdhpBATAAAAAHyGCEtOSfeqpmW8aA4ZE-AyadMK";
$recaptchaSecret = "6LdhpBATAAAAAAFAqsghIpZnsX5nR6hJUAQrbjj9";
$recaptchaVerifyUrl = "https://www.google.com/recaptcha/api/siteverify";
$recaptchaVerifyUrl .= "?secret=" . $recaptchaSecret;
$recaptchaVerifyUrl .= "&response=" . $_POST['g-recaptcha-response'];
$recaptchaVerifyUrl .= "&remoteip=" . $_SERVER['REMOTE_ADDR'];

$recaptchaResponse = json_decode(file_get_contents($recaptchaVerifyUrl));

if (isset($recaptchaResponse->success) && $recaptchaResponse->success == true) {

    spl_autoload_register(function ($class) {
        include '../lib/' . $class . '.php';
    });

    $ustanovaID = $_POST["ustanoveSelect"];

    if($ustanovaID == 318){
        $upz = new UPZEdgesSOIRI();
    }else{
        $upz = new UPZEdgesANY($ustanovaID);
    }

    $edges = $upz->getEdges();

    $edgesFormated = implode("\n", $edges);

    $edgesFileName = createEdgesFile($edgesFormated, $ustanovaID);

    $pythonResults = doPython3NetworkXscript($edgesFileName);

    $na = "N/A";

    $globalnaAnaliza = array(
        "globAnaliza_brRadova" => $upz->getBrRadova(),
        "globAnaliza_brRadovaSJednimAutorom" => $upz->getBrRadovaSJednimAutorom(),
        "globAnaliza_brRadovaSViseAutora" => $upz->getBrRadovaSViseAutora(),
        "globAnaliza_brAutora" => $upz->getBrAutora(),
        "globAnaliza_N" => $pythonResults->N,
        "globAnaliza_KK" => $pythonResults->KK,
        "globAnaliza_SS" => $pythonResults->SS,
        "globAnaliza_k" => $pythonResults->k,
        "globAnaliza_s" => $pythonResults->s,
        "globAnaliza_e" => $pythonResults->e,
        "globAnaliza_d" => $pythonResults->d,
        "globAnaliza_omega" => $pythonResults->omega,
        "globAnaliza_L" => $pythonResults->L,
        "globAnaliza_DD" => $pythonResults->DD,
        "globAnaliza_R" => $pythonResults->R,
        "globAnaliza_T" => $pythonResults->T,
        "globAnaliza_C" => $pythonResults->C,
        "globAnaliza_A" => $pythonResults->A
    );

    $viz = array(
        "vizMreza" => $pythonResults->vizMreza,
        "vizHistStup" => $pythonResults->vizHistStup,
        "vizHistSnag" => $pythonResults->vizHistSnag,
        "vizHistGrup" => $pythonResults->vizHistGrup,
        "vizEgo" => $pythonResults->vizEgo
    );

    $visjs = array(
        "nodes" => $upz->getVisJsNodes(),
        "edges" => $upz->getVisJsEdges()
    );

    $return = array(
        "ustanovaID" => $ustanovaID,
        "edgesFormated" => $edgesFormated,
        "globalnaAnaliza" => $globalnaAnaliza,
        "edgesFile" => $edgesFileName,
        "stup" => $pythonResults->stup,
        "snag" => $pythonResults->snag,
        "medju" => $pythonResults->medju,
        "viz" => $viz,
        "visjs" => $visjs
    );

    die(json_encode($return));
} else {
    die("recaptchaError");
}