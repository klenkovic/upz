import sys
import networkx as nx
import json
import operator as op
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

try:
    edgesFile = sys.argv[1]
    timehash = sys.argv[2]
    generatedPath = sys.argv[3]
except IndexError:
    print("Nije dana putanja do edges datoteke i/ili timehash i/ili putanja do direktorija za generiranje")
    sys.exit(2)

g = nx.read_weighted_edgelist(edgesFile, delimiter=";")
# Ukupni broj cvorova
N = g.number_of_nodes()

# Ukupni broj bridova
K = g.number_of_edges()

# Ukupna snaga mreze
S = 0
for e in g.edges(data=True):
    S = S + e[2].get('weight')

# Prosjecni stupanj mreze
k = (2 * K) / float(N)

# Prosjecna snaga mreze
s = S / N

# Prosjecna selektivnost
suma = 0
for w in g.degree(weight='weight').items():
    suma = suma + (w[1]/g.degree(w[0]))
e = suma / N

# Gustoca mreze
d = nx.density(g)

# Broj komponenti
omega = nx.number_connected_components(g)

sg = max(nx.connected_component_subgraphs(g), key=len)

# Prosjecna duljina najkraceg puta
L = nx.average_shortest_path_length(sg)

# Dijametar
D = nx.diameter(sg)

# Radijus
R = nx.radius(sg)

# Tranzitivnost
T = nx.transitivity(g)

# Prosjecni koeficijent grupiranja
C = nx.average_clustering(g, weight="weight")

# Koeficijent asortativnosti
A = nx.degree_assortativity_coefficient(g)

# Lokalna analiza - Prvih 10 rankiranih cvorova po stupnju cvora
stup = []
for i, j in zip(g.degree(), g.degree().values()):
    stup.append([i, j])
stup = sorted(stup, key=op.itemgetter(1), reverse=True)
stup = stup[0:10]


# Lokalna analiza - Prvih 10 rankiranih cvorova po snazi cvora

snag = []
for i in g.degree(weight='weight').items():
    snag.append(i)
snag = sorted(snag, key=op.itemgetter(1), reverse=True)
snag = snag[0:10]


# Lokalna analiza - Prvih 10 rankiranih cvorova po medjupolozenosti cvora

mp = nx.betweenness_centrality(g)
medju = []
for i, j in mp.items():
    medju.append([i, j])
medju = sorted(medju, key=op.itemgetter(1), reverse=True)
medju = medju[0:10]


# Vizualizacija mreze
pos = nx.spring_layout(g)
nx.draw(g, pos, node_color='#EEEEEE', edge_color='#000000', edge_cmap=plt.cm.Blues,  node_size=100, with_labels=True, font_size=5, width=1)
vizMreza = "vizMreza-" + timehash + ".png"
savepath = generatedPath + vizMreza
plt.savefig(savepath, dpi=300)
plt.clf()

# Stupanj cvora
stupanj_cvora = []
for i in stup:
    stupanj_cvora.append(i[1])

plt.loglog(stupanj_cvora, "b-", marker="o")
plt.ylabel("Stupnjevi čvorova")
plt.xlabel("Frekvencija")
vizHistStup = "vizHistStup-" + timehash + ".png"
savepath = generatedPath + vizHistStup
plt.savefig(savepath)
plt.clf()

# Snaga cvora
snaga_cvora = []
for i in snag:
    snaga_cvora.append(i[1])

plt.loglog(snaga_cvora, "b-", marker="o")
plt.ylabel("Snaga čvorova")
plt.xlabel("Frekvencija")
vizHistSnag = "vizHistSnag-" + timehash + ".png"
savepath = generatedPath + vizHistSnag
plt.savefig(savepath)
plt.clf()

# Koeficijent grupiranja
koeficijent_grupiranja = sorted(nx.clustering(g).values(), reverse=True)

plt.loglog(koeficijent_grupiranja, "b-", marker="o")
plt.ylabel("Koeficijent grupiranja")
plt.xlabel("Frekvencija")
vizHistGrup = "vizHistGrup-" + timehash + ".png"
savepath = generatedPath + vizHistGrup
plt.savefig(savepath)
plt.clf()

# Ego graf
(hub, stupanj) = sorted(g.degree().items(), key=op.itemgetter(1))[-1]

hub_ego = nx.ego_graph(g, hub)

pos = nx.spring_layout(hub_ego)

nx.draw(hub_ego, pos, node_color='#EEEEEE', node_size=100, with_labels=True, font_size=5, width=1)
nx.draw_networkx_nodes(hub_ego, pos, nodelist=[hub], node_size=200, node_color='r')

vizEgo = "vizEgo-" + timehash + ".png"
savepath = generatedPath + vizEgo
plt.savefig(savepath, dpi=300)
plt.clf()

# Vracanje vrijednosti

vrati = {
    "N": N,
    "KK": K,
    "SS": S,
    "k": k,
    "s": s,
    "e": e,
    "d": d,
    "omega": omega,
    "L": L,
    "DD": D,
    "R": R,
    "T": T,
    "C": C,
    "A": A,
    "stup": stup,
    "snag": snag,
    "medju": medju,
    "vizMreza": vizMreza,
    "vizHistStup": vizHistStup,
    "vizHistSnag": vizHistSnag,
    "vizHistGrup": vizHistGrup,
    "vizEgo": vizEgo
}

print(json.dumps(vrati, separators=(',', ': ')))
